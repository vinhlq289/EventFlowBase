﻿using EventFlow.Core;

namespace AggregateIdentity
{
    public class SchoolId :
        Identity<SchoolId>
    {
        public SchoolId(string value) : base(value) { }
    }
}