﻿using Aggregate;
using AggregateIdentity;
using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace EventModels
{
    [EventVersion("example", 1)]
    public class UserEvent :
        AggregateEvent<UserAggregate, UserId>
    {
        public UserEvent(int magicNumber)
        {
            MagicNumber = magicNumber;
        }

        public int MagicNumber { get; }
    }
}