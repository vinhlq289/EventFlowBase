﻿using Aggregate;
using AggregateIdentity;
using Command;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace CommandHandler
{
    public class UserCommandHandler :
        CommandHandler<UserAggregate, UserId, IExecutionResult, UserCommand>
    {
        public override Task<IExecutionResult> ExecuteCommandAsync(
            UserAggregate aggregate,
            UserCommand command,
            CancellationToken cancellationToken)
        {
            var executionResult = aggregate.SetMagicNumer(command.MagicNumber);
            return Task.FromResult(executionResult);
        }
    }
}