﻿using Aggregate;
using AggregateIdentity;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;

namespace Command
{
    public class SchoolCommand :
        Command<SchoolAggregate, SchoolId, IExecutionResult>
    {
        public SchoolCommand(
            SchoolId aggregateId,
            int magicNumber)
            : base(aggregateId)
        {
            MagicNumber = magicNumber;
        }

        public int MagicNumber { get; }
    }
}