﻿using Aggregate;
using AggregateIdentity;
using EventFlow.Aggregates;
using EventFlow.ReadStores;
namespace ReadModels
{
    public class UserReadModel :
        IReadModel,
        IAmReadModelFor<UserAggregate, UserId, UserEvent>
    {
        public int MagicNumber { get; private set; }

        public void Apply(
            IReadModelContext context,
            IDomainEvent<UserAggregate, UserId, UserEvent> domainEvent)
        {
            MagicNumber = domainEvent.AggregateEvent.MagicNumber;
        }
    }
}